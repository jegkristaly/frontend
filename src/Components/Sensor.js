import React, { Component } from 'react';
import SensorItem from "./SensorItem";
import {

  Grid,
  Form,
} from 'semantic-ui-react'

class Sensor extends Component {


  details(e) {
      var url = "https://sensor-backend.herokuapp.com/api/measurements/";
      fetch(url + this.props.currentId, {headers: {
          'Authorization': 'Bearer ' + this.props.accessToken
      }})
          .then(response => response.json())
          .then(response => {
        //  console.log(response);

              this.props.showDetails(response,this.props.currentId);

      });
clearInterval(this.interval);

  }

    addNew(e) {



        e.preventDefault();
        var url = "https://sensor-backend.herokuapp.com/api/sensors/";
        fetch(url, {
            method: "POST",
            headers: {"Content-Type": "application/json",
                'Authorization': 'Bearer ' + this.props.accessToken},
            body: JSON.stringify({
                name: this.refs.name.value,
                type: this.refs.type.value,
                distribution: this.refs.distribution.value,
                min: this.refs.min.value,
                max: this.refs.max.value
            })
        }).then(response => response.json())
            .then(response => {
            console.log(response);
            let sensorlist = this.props.sensors;
            sensorlist.push(response._id);
            console.log(sensorlist);


            var fetchurl= 'https://sensor-backend.herokuapp.com/api/measurements/' + this.props.currentId;

            fetch(fetchurl, {
                method: "PUT",
                headers: {"Content-Type": "application/json",
                    'Authorization': 'Bearer ' + this.props.accessToken},
                body: JSON.stringify({
                    sensors: sensorlist
                })

            });


        });

 this.interval = setInterval(this.details.bind(this), 5000);
    }

    render() {
        let sensorItems;
        if(this.props.sensors){
            sensorItems =this.props.sensors.map(sensor => {
                return(
                    <SensorItem accessToken={this.props.accessToken} currentId={this.props.currentId} key={sensor} sensor={sensor} />
                );
            });

        }
        if(this.props.sensors) {
            return (

                <div className="Sensor">


                   Sensors for measurement: {this.props.currentId}
                    {sensorItems}
                    <span class="alert-info">Use the Jégkristály application to modify sensors for this measurement.</span>
                    <br/>

                    <Grid.Column>

                                    <Form onSubmit={this.addNew.bind(this)}>
                                        <Form.Field>

                                            <input type="text" ref="name" placeholder="Sensor name"/>
                                            <select name="Type" ref="type" size="4">
                                                <option value="temperature">Temperature</option>
                                                <option value="humidity">Humidity</option>
                                                <option value="noise">Noise</option>
                                                <option value="light">Light</option>
                                            </select>
                                            <input type="number" ref="min" placeholder="Min"/>
                                            <input type="number" ref="max" placeholder="Max"/>
                                            <select name="Distribution" ref="distribution" size="2">
                                                <option value="linear">Linear</option>
                                                <option value="random">Random</option>
                                            </select>
                                            <button type="submit" class="pure-button pure-button-primary">Add Sensor</button>

                                        </Form.Field>
                                    </Form>
                                    </Grid.Column>
                </div>
            );
        }
        else{
            return(
                <div class="container" className="Sensor">
                <Grid.Column>
                    {sensorItems}
                    <br/>
                    </Grid.Column>

                </div>
            )
        }
    }
}

export default Sensor;
