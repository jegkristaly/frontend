import React, { Component } from 'react';
import Measurements from './Measurements';


class Login extends Component {

    constructor(){
        super();
        this.logIn = this.logIn.bind(this);
        this.state={
            loggedIn: false,
        };
        this.state={
            loggedOk: false,
        };
        this.state={
            register: false,
        };
        this.state={
          legend:"Please log in!",
        };
    }

    logIn(e) {
        e.preventDefault();
        if (this.refs.email.value === '' || this.refs.password.value === '') {
            alert('Please enter a valid email and password!')
        }
        else {
            var url = "https://sensor-backend.herokuapp.com/api";
            fetch(url + '/login', {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify({
                    username: this.refs.email.value,
                    password: this.refs.password.value
                })
            }).then(response => {

                if (response.ok) {
                  this.setState({
                      loginOk: true,
                  });
                    return response.json();
                } else {
                  //  throw new Error('login was not successful');
                    this.setState({legend: "Invalid email or password."})
                }
            }).then(json => {
              if(this.state.loginOk) {this.setState({
                    accessToken: json.token,
                });
                this.setState({
                    loggedIn: true,
                });
}
            })
        }
    }

    register(e) {
    this.setState({
        register: true,
    });
    }

    submitreg(e) {
        var url = "https://sensor-backend-dev.herokuapp.com/api";
        if (this.refs.email.value === '' || this.refs.password.value === '') {
            alert('Please enter a valid email and password!')
        }
        else {

            fetch(url + '/register', {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify({
                    username: this.refs.email.value,
                    password: this.refs.password.value
                })
            }).then(function (response) {
                console.log(response);
                if (response.ok) {
                }
            })

        }
    }

    render() {
        if(!this.state.loggedIn &&!this.state.register) {
            return (
                <div className="Login">
                    <form class="pure-form" onSubmit={this.logIn.bind(this)}>
                        <fieldset>
                            <legend>{this.state.legend}</legend>

                            <input ref="email" type="text" placeholder="E-mail cím"/>
                            <input ref="password" type="password" placeholder="Jelszó"/>

                            <button type="submit" class="btn pure-button pure-button-primary">Sign in</button>
                        </fieldset>
                    </form>
                    <button onClick={this.register.bind(this)} class="button-secondary pure-button">Dont have an account?</button>
                </div>
            );
        }
        if(this.state.register){
            return (
                <div className="Login">
                    <form class="pure-form" onSubmit={this.submitreg.bind(this)}>
                        <fieldset>
                            <legend>Regisztráció</legend>

                            <input ref="email" type="email" placeholder="E-mail cím"/>
                            <input ref="password" type="password" placeholder="Jelszó"/>

                            <button type="submit" class="pure-button pure-button-primary">Register</button>
                        </fieldset>
                    </form>
                </div>
            );
        }
        if(this.state.loggedIn){
            return (
                <div className="Login">


                    <Measurements accessToken={this.state.accessToken} creator={this.refs.email.value}/>




                </div>
            );
        }
    }

}

export default Login;
