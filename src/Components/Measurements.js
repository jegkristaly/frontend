import React, { Component } from 'react';
import MeasurementItem from "./MeasurementItem";
import Sensor from "./Sensor";
import {
  Button,
  Grid,
  Header,


  Message,

  Table,
  Form,
} from 'semantic-ui-react'

class Measurements extends Component {

    constructor(){
        super();



        this.state={
            measurements: [],
            mName: {},
            mStatus: {},
            mCreator: {},
            details: {},
            info: "",
            deleteinfo:""
        };
    }


    componentWillMount() {


        var url = "https://sensor-backend.herokuapp.com/api/measurements";
        fetch(url, {
            headers: {
                'Authorization': 'Bearer ' + this.props.accessToken
            }})
            .then(response => response.json())
            .then(response => {

                for(var i=0; i<response.length;i++){

                    let measurements=this.state.measurements;
                    measurements.push(response[i]);
                    this.setState({measurements:measurements});

                }
                });


    }


    tick(){

        var url = "https://sensor-backend.herokuapp.com/api/measurements";
        this.setState({measurementItems:[]});
        this.setState({measurements:[]});
        fetch(url, {
            headers: {
                'Authorization': 'Bearer ' + this.props.accessToken
            }})
            .then(response => response.json())
            .then(response => {

                for(var i=0; i<response.length;i++){

                    let measurements=this.state.measurements;
                    measurements.push(response[i]);
                    this.setState({measurements:measurements});

                }
            });
            this.setState({deletedinfo:""});
            clearInterval(this.interval);
    }

    componentDidMount(){

    //    this.interval = setInterval(this.tick.bind(this), 5000);

//clearInterval(this.interval);

    }

    componentWillUnmount(){
    //    clearInterval(this.interval);

    }

    showDetails(e,id) {
        console.log(e);
        this.setState({details: e});
        this.setState({currentId: id});

    }

    addNew(e) {
        e.preventDefault();
        var url = "https://sensor-backend.herokuapp.com/api/measurements";
        fetch(url, {
            method: "POST",
            headers: {"Content-Type": "application/json",
                'Authorization': 'Bearer ' + this.props.accessToken},
            body: JSON.stringify({
                creator: "hardCodedTODO",
                name: this.refs.name.value,
                duration: this.refs.duration.value
            })
        }).then(response => {
             console.log(response);
             this.setState({info:<Message positive>Measurement added!</Message>});
        });

       this.interval = setInterval(this.tick.bind(this), 1000);

    }
    deleted(){
     this.setState({deletedinfo:<Message warning>Measurement deleted!</Message>});
     this.interval = setInterval(this.tick.bind(this), 1000);
    }

    render() {



        let measurementItems;
        if(this.state.measurements){
            measurementItems =this.state.measurements.map(measurement => {
              //  console.log(measurement);
                return(
                    <MeasurementItem accessToken={this.props.accessToken} deleted={this.deleted.bind(this)} showDetails = {this.showDetails.bind(this)} key={measurement._id} measurement={measurement} />
                );
            });


        }
/*
        return (
            <div class="container" className="Measurements">
<aside >

                <table  class="pure-table pure-table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Delete</th>
                        <th>Details</th>
                    </tr>
                    </thead>


                    {measurementItems }


                </table>
<div>
{this.state.info}
                <form class="pure-form pure-form-stacked" onSubmit={this.addNew.bind(this)}>
                    <fieldset>

                        <input type="text" ref="name" placeholder="Measurement name"/>
                            <input type="text" ref="duration" placeholder="Duration"/>
                        <button type="submit" class="pure-button pure-button-primary">Add Measurement</button>

                    </fieldset>
                </form>
                </div>
</aside >
<div class="container">
<h3><span class="label label-info">Measurement: {this.state.details.name}</span></h3>

                <br/>
                {this.state.details.duration}
                <br/>
                {this.state.details.creator}
                <br/>
                <br/>
                <Sensor accessToken={this.props.accessToken} sensors={this.state.details.sensors} currentId={this.state.currentId}/>
</div>
            </div>


        );
*/

return(
  <Grid container style={{ padding: '5em 0em' }}>
    <Grid.Row>
      <Grid.Column>
        <Header as='h1' dividing>Project Jégkristály</Header>
      </Grid.Column>
    </Grid.Row>






    <Grid.Row>
      <Grid.Column>
        <Header as='h1'>Measurements</Header>

        <Grid columns={2}>
                  <Grid.Column>
                  <Form onSubmit={this.addNew.bind(this)}>
                      <Form.Field>

                          <input type="text" ref="name" placeholder="Measurement name"/>
                              <input type="text" ref="duration" placeholder="Duration"/>
                          <button type="submit" class="pure-button pure-button-primary">Add Measurement</button>

                      </Form.Field>
                  </Form>
                  {this.state.info}


                  </Grid.Column>

                    <Grid.Column>
                      <Table basic>

                          <thead>
                          <tr>
                              <th>Name</th>
                              <th>Status</th>
                              <th>Delete</th>
                              <th>Details</th>
                          </tr>
                          </thead>


                          {measurementItems }



                      </Table>
                      {this.state.deletedinfo} <br/>
                      <Button onClick={this.tick.bind(this)} as='a' primary tabindex='0'>Refresh</Button>
                    </Grid.Column>
                    </Grid>
                  </Grid.Column>





    </Grid.Row>
    <Grid.Row>
    <Grid columns={2}>
    <Grid.Column>
<Sensor accessToken={this.props.accessToken} sensors={this.state.details.sensors} currentId={this.state.currentId} showDetails = {this.showDetails.bind(this)}/>
</Grid.Column>
</Grid>
</Grid.Row>


  </Grid>

);
    }
}

export default Measurements;
