import React, { Component } from 'react';
import Chart from './Chart';
import {

  Grid,

} from 'semantic-ui-react'
class SensorItem extends Component {

    constructor(props){
        super(props);

        this.state={
            response: [
                {
                    name: '',
                    min: '',
                    max: '',
                    distribution: ''
                }
            ],
            valid: false,
            chartData: {}
        };


    }


    componentWillMount() {
  this.setState({mId:this.props.currentId});
        var url = "https://sensor-backend.herokuapp.com/api/sensors/";
        fetch(url+this.props.sensor, {
            method: "GET",
            headers: {
                'Authorization': 'Bearer ' + this.props.accessToken
            }})

            .then(response =>{
                console.log(response);
                if(response.ok) {
                if(response.status===200){
                  this.setState({valid:true});
                  return response.json();
                }
                }})
                .then(response => {
               this.setState({response:response});
                });




    }



    render() {
if(this.state.valid){
        return (
          <Grid.Column>
            <div className="SensorItem">


                {this.state.response.name}

                <br/>
                <Chart sensor={this.props.sensor} currentId={this.props.currentId} accessToken={this.props.accessToken} type={this.state.response.type}/>
            </div>
            </Grid.Column>
        );
      }
      else{
        return(
          <div className="SensorItem">
          <span class="bg-danger">Sensor {this.props.sensor} invalid or deleted.</span>
{this.props.currentId}


          </div>
        );
      }
    }
  }



export default SensorItem;
