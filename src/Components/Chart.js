import React, {Component} from "react";
import {Line} from "react-chartjs-2";
//import moment from 'moment';
import {
  Button,
} from 'semantic-ui-react'


class Chart extends Component{
  constructor(props){
    super(props);
    this.state={
      chartData: {
        labels: [],
        datasets:[
          {
            data:[]
          }
        ]
      }
    }

    this.state={data: []}
    this.state={labels: []}
    this.state={valid: false}
    this.state={labelmin: 999999999999}
    this.state={labelmax: 100000}
    this.state={stop: false}
    var url = "https://sensor-backend.herokuapp.com/api/sensors/";

        fetch(url+this.props.sensor+ "/data/" + this.props.currentId, {
            method: "GET",
            headers: {
                'Authorization': 'Bearer ' + this.props.accessToken
            }})

            .then(response =>{
          //      console.log(response);
                if(response.ok) {
                if(response.status===200){
                  this.setState({valid:true});
                  return response.json();


    }

                }})
                .then(response => {
            let data=[this.state.data];
            let labels=[];
            if(this.state.valid){
              this.setState({labelmin:response.data[0].timeStamp});
              this.setState({labelmax:response.data[response.data.length-1].timeStamp});
              this.refs.start.value = this.state.labelmin;
              this.refs.end.value=this.state.labelmax;
                for(var i = 0; i < response.data.length; i++){
                  /*  if((response.data[i].timeStamp)<(this.state.labelmin)){
                      this.setState({labelmin:response.data[i].timeStamp})
                    }*/
                /*    if((response.data[i].timeStamp)>(this.state.labelmax)){
                      this.setState({labelmax:response.data[i].timeStamp})
                    }*/

                    data.push(response.data[i].value);
            //      labels.push(moment.unix(response.data[i].timeStamp).format("MM/DD/YYYY"));
            var t = new Date(response.data[i].timeStamp);



labels.push(t.toTimeString().split(' ')[0]);
            //      moment.unix(value).format("MM/DD/YYYY");


              this.setState({data:data});
              this.setState({labels:labels});
            }}
            this.setState({valid:false});
this.setState({      chartData: {
        labels: labels,
        datasets:[
          {
            label: this.props.type,
            data:data
          }
        ]
      }})
console.log(this.state.chartData);
                });
  }


tick(){
  this.state={data: []}
  this.state={labels: []}
  var url = "https://sensor-backend.herokuapp.com/api/sensors/";

      fetch(url+this.props.sensor+ "/data/" + this.props.currentId, {
          method: "GET",
          headers: {
              'Authorization': 'Bearer ' + this.props.accessToken
          }})

          .then(response =>{
              if(response.ok) {
              if(response.status===200){
                this.setState({valid:true});
                return response.json();

  }

              }})
              .then(response => {
          let data=[];
          let labels=[];
          if(this.state.valid){
              for(var i = 0; i < response.data.length; i++){
                if((response.data[i].timeStamp)<(this.state.labelmin)){
                  this.setState({labelmin:response.data[i].timeStamp})
                }
                if((response.data[i].timeStamp)>(this.state.labelmax)){
                  this.setState({labelmax:response.data[i].timeStamp})
                }


                if(response.data[i].timeStamp>=this.refs.start.value && response.data[i].timeStamp<=this.refs.end.value)  {
                  data.push(response.data[i].value);
                  var t = new Date(response.data[i].timeStamp);
                  labels.push(t.toTimeString().split(' ')[0]);
}
            this.setState({data:data});
            this.setState({labels:labels});
          }}
      //    this.refs.start.value = this.state.labelmin;
      if(!this.state.stop){this.refs.end.value=this.state.labelmax;}
          this.setState({valid:false});
this.setState({      chartData: {
      labels: labels,
      datasets:[
        {
          label: this.props.type,
          data:data
        }
      ]
    }})
              });
}

  componentDidMount(){

      this.interval = setInterval(this.tick.bind(this), 5000);

  }

  componentWillUnmount(){
      clearInterval(this.interval);

  }

stop(){
  this.setState({stop: true});
}

start(){
  this.setState({stop:false});
}

render(){
  if(!this.state.stop){
  return (
    <div ClassName="Chart">
    <form>
      <input ref="start" type="range" name="start" min={this.state.labelmin} max={this.state.labelmax}/>
    </form>
    <form>
      <input ref="end" type="range" name="start" min={this.state.labelmin} max={this.state.labelmax}/>
    </form>
    <Button onClick={this.stop.bind(this)} as='a' primary tabindex='0'>Stop Refreshing</Button>
    <Line
    data={this.state.chartData}

    />
    </div>
  )
}
else{
  return (
    <div ClassName="Chart">
    <form>
      <input ref="start" type="range" name="start" min={this.state.labelmin} max={this.state.labelmax}/>
    </form>
    <form>
      <input ref="end" type="range" name="start" min={this.state.labelmin} max={this.state.labelmax}/>
    </form>
    <Button onClick={this.start.bind(this)} as='a' primary tabindex='0'>Start Refreshing</Button>
    <Line
    data={this.state.chartData}

    />
    </div>
  )
}
}


}
export default Chart;
