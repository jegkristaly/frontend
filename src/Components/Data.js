import React, { Component } from 'react';
import fetch from 'isomorphic-fetch';



class Data extends Component {

    constructor(){
        super();
        this.state={};
    }

    componentWillMount() {

        var url = "https://murmuring-caverns-85034.herokuapp.com/api";
        fetch(url)
            .then(response => response.json())
            .then(response => {
            this.setState({
                data: response.message
            });
            });

    }

    render() {
        return (
            <div className="Data">
                {this.state.data}
            </div>
        );
    }
}

export default Data;
