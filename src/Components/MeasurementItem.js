import React, { Component } from 'react';


class MeasurementItem extends Component {


    componentWillMount() {

    }

    delete(e) {
        var url = "https://sensor-backend.herokuapp.com/api/measurements/";
        fetch(url + this.props.measurement._id, {
            method: 'delete',
            headers: {
                'Authorization': 'Bearer ' + this.props.accessToken
            }
        }).then(response => {
            console.log(response);
        });

        this.props.deleted();

    }



    details(e) {
        var url = "https://sensor-backend.herokuapp.com/api/measurements/";
        fetch(url + this.props.measurement._id, {headers: {
            'Authorization': 'Bearer ' + this.props.accessToken
        }})
            .then(response => response.json())
            .then(response => {
          //  console.log(response);

                this.props.showDetails(response,this.props.measurement._id);
        });


    }

    render() {

        if(this.props.measurement.live)
        {
            return (

                    <tbody>
                    <tr>
                        <td>{this.props.measurement.name}</td>
                        <td><span class="text-success">LIVE</span></td>
                        <td><button onClick={this.delete.bind(this)} class="button-error pure-button">Delete<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></button></td>
                        <td><button onClick={this.details.bind(this)} class="pure-button">
                            <i class="fa fa-info-circle"></i>
                            Details<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                        </button></td>
                    </tr>
                    </tbody>







            );

        }
        else{
            return (


                <tbody>
                <tr>
                    <td>{this.props.measurement.name}</td>
                    <td><span class="text-muted">Stopped</span></td>
                    <td><button onClick={this.delete.bind(this)} class="button-error pure-button">Delete<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></button></td>
                    <td><button onClick={this.details.bind(this)} class="pure-button">
                        <i class="fa fa-info-circle"></i>
                        Details<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
                    </button></td>
                </tr>
                </tbody>



        );
        }
    }
}

export default MeasurementItem;
